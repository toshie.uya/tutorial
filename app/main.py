class Car:
    _start = False
    def __init__(self, name):
        self._name = name

    def start(self):
        if self._start:
            return self._name + " Already started"
        
        self._start = True
        return self._name + " Car started"

    def stop(self):
        if not self._start:
            return self._name + " Already stopped"
        
        self._start = False
        return self._name + " Car stop"

names = ["First", "Second", "Third", "Fourth"]
cars = []
for name in names:
    cars.append(Car(name))

print(cars[0].start())
print(cars[0].start())
print(cars[1].start())
print(cars[2].start())
print(cars[1].start())
print(cars[3].start())
print(cars[3].start())